﻿namespace MyAPI.Dtos.CompaniesDTO
{
    public class CompanyDTO
    {
        public string Name { get; set; } = null!;
        public string? Description { get; set; }
        public string? Location { get; set; }
        public string? Phone { get; set; }
        public string? Status { get; set; }
        public int ManagerID { get; set; }
        public string UserName { get; set; }
    }
}
