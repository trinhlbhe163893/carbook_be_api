﻿namespace MyAPI.Dtos.TicketDTO
{
    public class OutBookTicket
    {
        public decimal Price { get; set; }
        public DateTime? TimeFrom { get; set; }
        public DateTime? TimeTo { get; set; }
        public List<Select> SeatCode { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public string startLocation { get; set; }
        public string endLocation { get; set; }
        public string Name { get; set; } = null!;
        public int CarId { get; set; }



    }
    public class Select
    {
        public int id { get; set; }
        public string value { get; set; }
    }
}
