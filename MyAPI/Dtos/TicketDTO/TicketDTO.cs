﻿
using MyAPI.Models;

namespace MyAPI.Dtos.TicketDTO
{
    public class TicketDTO
    {
        public int CarId { get; set; }
        public decimal Price { get; set; }
        public DateTime? TimeFrom { get; set; }
        public DateTime? TimeTo { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
       

    }
}
