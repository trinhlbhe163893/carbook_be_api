﻿namespace MyAPI.Dtos.UserDTO
{
    public class UserCompanyDTO
    {
        public int ID { get; set; }
        public string Value { get; set; }
    }
}
