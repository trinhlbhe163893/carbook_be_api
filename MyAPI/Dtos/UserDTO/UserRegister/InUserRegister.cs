﻿using System.Text.Json.Serialization;

namespace MyAPI.Dtos.UserDTO.UserRegister
{
    public class InUserRegister
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; } = null!;
        public string? Picture { get; set; }
        public DateTime? Dob { get; set; }
        public string? Address { get; set; }
        public string? City { get; set; }
        public string? Acitve { get; private set; } = "Active";
    }
}
