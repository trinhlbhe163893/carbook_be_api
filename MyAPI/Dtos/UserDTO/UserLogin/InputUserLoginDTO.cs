﻿namespace MyAPI.Dtos.UserDTO.UserLogin
{
    public class InputUserLoginDTO
    {
        public string? username { get; set; }
        public string password { get; set; } = null!;
    }
}
