﻿using MyAPI.Dtos.RoleDTO;
using MyAPI.Models;
namespace MyAPI.Dtos.UserDTO.UserLogin
{
    public class OutUserLoginDTO
    {
        public int UserId { get; set; }
        public string UserName { get; set; } = null!;
        public string? Email { get; set; }
        public string? Password { get; set; }
        public List<UserRoleDTO> UserRoles { get; set; }
        public string Token { get; set; } = "";

    }
}
