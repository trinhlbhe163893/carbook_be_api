﻿namespace MyAPI.Dtos.UserDTO
{
    public class UserDTO
    {
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string? Email { get; set; }
        public string Mobile { get; set; } = null!;
        public DateTime? Dob { get; set; }
        public string? Picture { get; set; }
        public string? Address { get; set; }
        public string? City { get; set; }
        public string Password { get; set; } = null!;
        public int RoleId { get; set; } 
    }
}
