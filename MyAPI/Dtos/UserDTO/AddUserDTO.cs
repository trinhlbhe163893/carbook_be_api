﻿using MyAPI.Models;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace MyAPI.Dtos.UserDTO
{
 
    public class AddUserDTO
    {
        public int UserId { get; set; }
        [Required]
        public string UserName { get; set; } = null!;

        [Required]
        [MaxLength(255)]
        [JsonPropertyName("email")]
        public string? Email { get; set; }

        [JsonPropertyName("mobile")]
        public string Mobile { get; set; } = null!;

        [Required]
        [MaxLength(255)]
        [JsonPropertyName("password")]
        public string Password { get; set; } = null!;

        [Required]
        [JsonPropertyName("dob")]
        public DateTime? Dob { get; set; }

        [Required]
        [MaxLength(255)]
        [JsonPropertyName("address")]
        public string? Address { get; set; }

        [Required]
        [MaxLength(255)]
        [JsonPropertyName("city")]
        public string? City { get; set; }

        [Required]
        [JsonPropertyName("status")]
        public string? Status { get; set; }

       
    }
}
