﻿namespace MyAPI.Dtos.SchduleDTO
{
    public class ScheduleDTO
    {
        public int CompanyId { get; set; }
        public string Name { get; set; } = null!;
        public string? StartLocation { get; set; }

        public string? EndLocation { get; set; }
        public string? Status { get; set; }
    }
}
