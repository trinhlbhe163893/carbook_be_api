﻿namespace MyAPI.Dtos.ScheduleDTO
{
    public class SchdeuleListDTO
    {
        public int CompanyId { get; set; }

        public string CompanyName { get; set; }
        public string Name { get; set; } = null!;
        public string? Location { get; set; }
        public string? Status { get; set; }
    }
}
