﻿namespace MyAPI.Dtos.CarDTO
{
    public class CarDTO
    {
        public int CompanyId { get; set; }
        public string Number { get; set; } = null!;
        public string? SeatCode { get; set; }
        public string? Phone { get; set; }
        public int? TypeId { get; set; }
        public string? Status { get; set; }
    }
}
