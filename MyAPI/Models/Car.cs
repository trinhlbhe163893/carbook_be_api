﻿using System;
using System.Collections.Generic;

namespace MyAPI.Models
{
    public partial class Car
    {
        public Car()
        {
            Tickets = new HashSet<Ticket>();
        }

        public int CarId { get; set; }
        public int CompanyId { get; set; }
        public string Number { get; set; } = null!;
        public string? SeatCode { get; set; }
        public string? Phone { get; set; }
        public int? TypeId { get; set; }
        public string? Status { get; set; }

        public virtual Company Company { get; set; } = null!;
        public virtual ICollection<Ticket> Tickets { get; set; }
    }
}
