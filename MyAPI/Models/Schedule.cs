﻿using System;
using System.Collections.Generic;

namespace MyAPI.Models
{
    public partial class Schedule
    {
        public Schedule()
        {
            Tickets = new HashSet<Ticket>();
        }

        public int ScheduleId { get; set; }
        public int CompanyId { get; set; }
        public string Name { get; set; } = null!;
        public string? Location { get; set; }
        public string? Status { get; set; }

        public virtual Company Company { get; set; } = null!;
        public virtual ICollection<Ticket> Tickets { get; set; }
    }
}
