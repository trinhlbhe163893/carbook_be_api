﻿using System;
using System.Collections.Generic;

namespace MyAPI.Models
{
    public partial class Alert
    {
        public int AlertId { get; set; }
        public int TicketId { get; set; }
        public string Title { get; set; } = null!;
        public string Content { get; set; } = null!;
        public int? Amount { get; set; }
        public int? TypeId { get; set; }
        public int? Frequency { get; set; }
        public DateTime? Date { get; set; }
        public string? Status { get; set; }

        public virtual Ticket Ticket { get; set; } = null!;
    }
}
