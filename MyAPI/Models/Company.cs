﻿using System;
using System.Collections.Generic;

namespace MyAPI.Models
{
    public partial class Company
    {
        public Company()
        {
            Cars = new HashSet<Car>();
            Schedules = new HashSet<Schedule>();
        }

        public int CompanyId { get; set; }
        public int? ManagerId { get; set; }
        public string Name { get; set; } = null!;
        public string? Description { get; set; }
        public string? Location { get; set; }
        public string? Phone { get; set; }
        public string? Status { get; set; }

        public virtual User? Manager { get; set; }
        public virtual ICollection<Car> Cars { get; set; }
        public virtual ICollection<Schedule> Schedules { get; set; }
    }
}
