﻿using System;
using System.Collections.Generic;

namespace MyAPI.Models
{
    public partial class Review
    {
        public Review()
        {
            InverseReviewNavigation = new HashSet<Review>();
        }

        public int ReviewId { get; set; }
        public int UserId { get; set; }
        public int TicketId { get; set; }
        public int? ReplyId { get; set; }
        public string? Comment { get; set; }
        public int? Rating { get; set; }
        public DateTime? Date { get; set; }
        public string? Status { get; set; }

        public virtual Review? ReviewNavigation { get; set; }
        public virtual Ticket Ticket { get; set; } = null!;
        public virtual User User { get; set; } = null!;
        public virtual ICollection<Review> InverseReviewNavigation { get; set; }
    }
}
