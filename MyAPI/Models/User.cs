﻿using System;
using System.Collections.Generic;

namespace MyAPI.Models
{
    public partial class User
    {
        public User()
        {
            Companies = new HashSet<Company>();
            Reviews = new HashSet<Review>();
            Tickets = new HashSet<Ticket>();
            UserRoles = new HashSet<UserRole>();
        }

        public int UserId { get; set; }
        public string UserName { get; set; } = null!;
        public string? Email { get; set; }
        public string Mobile { get; set; } = null!;
        public string Password { get; set; } = null!;
        public string? Picture { get; set; }
        public DateTime? Dob { get; set; }
        public string? Address { get; set; }
        public string? City { get; set; }
        public string? Status { get; set; }

        public virtual ICollection<Company> Companies { get; set; }
        public virtual ICollection<Review> Reviews { get; set; }
        public virtual ICollection<Ticket> Tickets { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}
