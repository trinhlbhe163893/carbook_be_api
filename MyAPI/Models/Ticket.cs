﻿using System;
using System.Collections.Generic;

namespace MyAPI.Models
{
    public partial class Ticket
    {
        public Ticket()
        {
            Alerts = new HashSet<Alert>();
            Reviews = new HashSet<Review>();
        }

        public int TicketId { get; set; }
        public int? UserId { get; set; }
        public int CarId { get; set; }
        public int ScheduleId { get; set; }
        public string SeatCode { get; set; } = null!;
        public decimal Price { get; set; }
        public DateTime? TimeFrom { get; set; }
        public DateTime? TimeTo { get; set; }
        public DateTime? DateFrom { get; set; }
        public DateTime? DateTo { get; set; }
        public string? Status { get; set; }

        public virtual Car Car { get; set; } = null!;
        public virtual Schedule Schedule { get; set; } = null!;
        public virtual User? User { get; set; }
        public virtual ICollection<Alert> Alerts { get; set; }
        public virtual ICollection<Review> Reviews { get; set; }
    }
}
