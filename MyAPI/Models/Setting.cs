﻿using System;
using System.Collections.Generic;

namespace MyAPI.Models
{
    public partial class Setting
    {
        public int SettingId { get; set; }
        public string Type { get; set; } = null!;
        public string Table { get; set; } = null!;
        public string Title { get; set; } = null!;
        public string? Description { get; set; }
        public string? Status { get; set; }
    }
}
