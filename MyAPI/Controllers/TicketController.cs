﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MyAPI.DAO;
using MyAPI.Dtos.TicketDTO;
using MyAPI.Models;
using System.Collections;
using System.Net.Sockets;
using System.Security.Cryptography.X509Certificates;

namespace MyAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TicketController : ControllerBase
    {


        private readonly TicketDAO _ticketDAO;
        public TicketController(TicketDAO ticketDAO)
        {
            _ticketDAO = ticketDAO;
        }
        // create ticket
        [Authorize(Roles = "3,2")]
        [HttpPost("createTicket/{number}/{carId}")]
        public IActionResult CreateTicket(TicketDTO ticketDto, int number, int carId)
        {

            _ticketDAO.createTicket(ticketDto, number, carId);
            return Ok();
        }
        [Authorize]
        [HttpPost("changeStatusTicket")]
        public IActionResult ChangeStatusTicket(CancleOrBookTicketDTO[] listTicketID)
        {
          
            for (int i = 0; i < listTicketID.Length; i++)
            {
                _ticketDAO.BookOrCancleTicket(listTicketID[i].TicketId);

            }

            return Ok();
        }


        [HttpGet("{startLocation}/{endLocation}/{dateTime}")]
        public IActionResult GetTicket(string startLocation, string endLocation, DateTime dateTime)
        {

            List<OutBookTicket> ticket = _ticketDAO.getTicketByRequire(startLocation, endLocation, dateTime);
            return Ok(ticket);
        }
        [HttpGet("startlocation")]
        public IActionResult GetStartLocation()
        {
            List<string> location = _ticketDAO.GetLocation();
            List<Location> Startlocation = _ticketDAO.GetStartLocation(location);
            string display = _ticketDAO.ConvertToJson(Startlocation);
            return Ok(display);
        }
        [HttpGet("endlocation")]
        public IActionResult GetEndLocation()
        {
            List<string> location = _ticketDAO.GetLocation();
            List<Location> Endlocation = _ticketDAO.GetEndLocation(location);
            string display = _ticketDAO.ConvertToJson(Endlocation);
            return Ok(display);
        }


    }
}
