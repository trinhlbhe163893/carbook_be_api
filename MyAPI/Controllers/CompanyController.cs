﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MyAPI.DAO;
using MyAPI.Dtos.CompaniesDTO;
using MyAPI.Models;


namespace MyAPI.Controllers
{
  
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyController : ControllerBase
    {
        private readonly CompanyDAO _compayDAO;
        
        public CompanyController(CompanyDAO compayDAO) 
        {
            _compayDAO = compayDAO;
        }
        [Authorize(Roles = "3,2")]
        [HttpPost("update/{id}")]
        public IActionResult updateCompay(int id, CompanyDTO companyDTO)
        {
            if (!ModelState.IsValid) 
            {
                return BadRequest(ModelState);
            }
            bool checkNameCompany = _compayDAO.checkNameCompanyExist(companyDTO);
            if (!checkNameCompany) 
            {
                return BadRequest("Name Company is exsit in database");
            }
            _compayDAO.updateCompanyById(id, companyDTO);
            return Ok(companyDTO);
        }
        [HttpPost("addCompany")]
        [Authorize(Roles = "3,2")]
        public IActionResult addCompany(CompanyDTO companyDTO) 
        {
            //var userId = User.Claims.FirstOrDefault(u => u.Type == "UserId")?.Value;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            bool checkName = _compayDAO.checkNameCompanyExist(companyDTO);
            if (!checkName)
            {
                return BadRequest("Name Company is exsit");
            }
            _compayDAO.addCompany(companyDTO);

            return Ok(companyDTO);
        }
        [Authorize]
        [HttpPost("changeStatusCompany/{id}")]
        public IActionResult ChangeStatusCompany(int id) 
        {
            if(id == null)
            {
                return BadRequest("id not null");
            }
            var companyById = _compayDAO.GetCompanyById(id);
            if(companyById == null)
            {
                return BadRequest("Not found company");
            }
            _compayDAO.ChangeStatusCompany(id);
            return Ok(companyById);
        }
        [Authorize]
        [HttpGet("listcompany")]
        public IActionResult GetCompany()
        {
            var listCompany = _compayDAO.GetCompanies();
            if (listCompany == null)
            {
                return NotFound();
            }
            return Ok(listCompany);
        }

    }
}
