﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MyAPI.DAO;
using MyAPI.Dtos.CompaniesDTO;
using MyAPI.Dtos.SchduleDTO;

namespace MyAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ScheduleController : Controller
    {
        private readonly ScheduleDAO _scheduleDAO;
        public ScheduleController(ScheduleDAO scheduleDAO)
        {
            _scheduleDAO = scheduleDAO;
        }
        [Authorize(Roles = "3,2")]
        [HttpPost("Update/{id}")]
        public IActionResult updateSchedule(int id, ScheduleDTO scheduleDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            bool checkNameSchedule = _scheduleDAO.checkNameScheduleExist(scheduleDTO);
            if (!checkNameSchedule)
            {
                return BadRequest("Name Schedule is exsit in database");
            }
            _scheduleDAO.updateScheduleById(id, scheduleDTO);
            return Ok(scheduleDTO);
        }
        [Authorize(Roles = "3,2")]
        [HttpPost("ListSchedule")]
        public IActionResult listSchedule()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var listSchedule = _scheduleDAO.GetSchedules();
            if (listSchedule == null)
            {
                return BadRequest("Schedule has null in database");
            }
            return Ok(listSchedule);
        }
        [Authorize(Roles = "3,2")]
        [HttpPost("AddSchedule")]
        public IActionResult addSchedule(ScheduleDTO scheduleDTO)
        {
            var userId = User.Claims.FirstOrDefault(u => u.Type == "UserId")?.Value;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            bool checkName = _scheduleDAO.checkNameScheduleExist(scheduleDTO);
            if (!checkName)
            {
                return BadRequest("Name Schedule is exsit");
            }
            _scheduleDAO.addSchedule(scheduleDTO);

            return Ok(scheduleDTO);
        }
        [Authorize(Roles = "3,2")]
        [HttpPost("changeStatusSchedule")]
        public IActionResult ChangeStatusSchedule(int id)
        {
            if (id == null)
            {
                return BadRequest("id not null");
            }
            var companyById = _scheduleDAO.GetScheduleById(id);
            if (companyById == null)
            {
                return BadRequest("Not found schedule");
            }
            _scheduleDAO.ChangeStatusSchedule(id);
            return Ok(companyById);
        }
    }
}
