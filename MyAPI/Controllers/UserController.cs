﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
//using MyAPI.Token;
using MyAPI.DAO;
using MyAPI.Dtos.UserDTO.UserLogin;
using MyAPI.Dtos.UserDTO.UserRegister;
using Microsoft.AspNetCore.Authorization;
using System.IdentityModel.Tokens.Jwt;
using MyAPI.Models;
using MyAPI.Dtos.UserDTO;
using Microsoft.IdentityModel.Tokens;

namespace MyAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        
        private readonly UserDAO _userDAO;
        private readonly IMapper _mapper;
        private IConfiguration _configuration;

        public UserController(IMapper mapper, IConfiguration configuration, UserDAO userDAO)
        {
            _mapper = mapper;
            _configuration = configuration;
            _userDAO = userDAO;
        }

        [HttpPost("register")]
        public IActionResult Register(InUserRegister inUserRegister)
        {   
            if (inUserRegister == null)
            {
                return BadRequest();
            }
            bool checkAccountExsit = _userDAO.checkEmailAndUserName(inUserRegister);
            if (!checkAccountExsit)
            {
                return BadRequest();
            }
            _userDAO.registerUser(inUserRegister);
            return Ok();
        }


        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<IActionResult> LoginTest([FromBody] InputUserLoginDTO user)
        {
           

            OutUserLoginDTO loggedInUser = await _userDAO.Login(user.username,user.password);

            if (loggedInUser != null)
            {
                var response = new
                {
                    Authorization = $"{loggedInUser.Token}"
                };
                return Ok(response);
            }

            return BadRequest(new { message = "User login unsuccessful" });
        }
        [Authorize]
        [HttpGet("userprofile")]
        public IActionResult GetUserProfile()
        {
            string token = Request.Headers["Authorization"];
            if (token.StartsWith("Bearer"))
            {
                token = token.Substring("Bearer ".Length).Trim();
            }
            if (string.IsNullOrEmpty(token))
            {
                return BadRequest("Token is required.");
            }

            var handler = new JwtSecurityTokenHandler();
            try
            {
                var jwtToken = handler.ReadJwtToken(token);

                // Lấy claim "ID" từ token
                var userIdClaim = jwtToken.Claims.FirstOrDefault(c => c.Type == "ID");
                if (userIdClaim == null)
                {
                    return Unauthorized("Invalid token: ID claim is missing.");
                }

                // Chuyển đổi id từ chuỗi sang int
                if (!int.TryParse(userIdClaim.Value, out int userId))
                {
                    return BadRequest("Invalid user ID in token.");
                }

                // Lấy thông tin người dùng từ cơ sở dữ liệu
                var user = _userDAO.GetUserProfileById(userId);
                if (user == null)
                {
                    return NotFound();
                }

                return Ok(user);
            }
            catch (SecurityTokenException)
            {
                return Unauthorized("Invalid token.");
            }
        }
        [Authorize(Roles = "3,2")]
        [HttpGet("userrolecompany")]
        public IActionResult getUserRoleCompany()
        {
            var userRoleCompany = _userDAO.GetUserRoleCompany();
            if (userRoleCompany == null)
            {
                return NotFound();
            }
            return Ok(userRoleCompany);
        }
        [Authorize]
        [HttpPost("edituser/{id}")]
        public IActionResult editUser(int id, UserDTO user)
        {
            if (user == null)
            {
                return NotFound();
            }
            var check = _userDAO.checkDuplicateEmailOrPhone(id, user.Email, user.Mobile);
            if (check)
            {
                return BadRequest("Phone or email is exsit");
            }
            _userDAO.editUser(id, user);
            return Ok();
        }

    }
}
