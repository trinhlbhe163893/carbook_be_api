﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MyAPI.DAO;
using MyAPI.Dtos.CarDTO;
using MyAPI.Dtos.SchduleDTO;

namespace SampleWebApiAspNetCore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CarController : Controller
    {
        private readonly CarDAO _carDAO;
        public CarController(CarDAO carDAO)
        {
            _carDAO = carDAO;
        }
        [Authorize(Roles = "3,2")]
        [HttpPost("Update/id")]
        public IActionResult updateCar(int id, CarDTO carDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var checkNameCar = _carDAO.GetCarById(id);
            if (checkNameCar == null)
            {
                return BadRequest("Car is exsit in database");
            }
            _carDAO.updateCarById(id, carDTO);
            return Ok(carDTO);
        }
        [Authorize]
        [HttpPost("ListCar")]
        public IActionResult listCar()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var listSchedule = _carDAO.GetListCar();
            if (listSchedule == null)
            {
                return BadRequest("Car has null in database");
            }
            return Ok(listSchedule);
        }
        //[Authorize]

        [Authorize(Roles = "3,2")]
        [HttpPost("AddCar")]
        public IActionResult addCar(CarDTO carDTO)
        {
            var userId = User.Claims.FirstOrDefault(u => u.Type == "UserId")?.Value;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var checkCar = _carDAO.checkCarExistByNumber(carDTO.Number);
            if (!checkCar)
            {
                return BadRequest("Car is exsit");
            }
            _carDAO.addCar(carDTO);

            return Ok(carDTO);
        }

        [Authorize(Roles = "3,2")]
        [HttpPost("changeStatusCarById")]
        public IActionResult ChangeStatusCar(int id)
        {
            if (id == null)
            {
                return BadRequest("id not null");
            }
            var companyById = _carDAO.GetCarById(id);
            if (companyById == null)
            {
                return BadRequest("Not found car");
            }
            _carDAO.ChangeStatusCar(id);
            return Ok(companyById);
        }
        [Authorize]
        [HttpPost("listCompanyCar")]
        public IActionResult GetListCompanyAdd()
        {

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var listCompany = _carDAO.getCompany();
            if (listCompany == null)
            {
                return BadRequest("Company has null in database");
            }
            return Ok(listCompany);
        }
    }
}
