﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using MyAPI.Models;
using MyAPI.Dtos.TicketDTO;
using MyAPI.Dtos.CarDTO;
using System.Xml.Linq;
using Newtonsoft.Json;

namespace MyAPI.DAO
{
    public class TicketDAO
    {
        private readonly DB_CarBookingContext _context;
        private readonly IMapper _mapper;
        public TicketDAO(DB_CarBookingContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        //get number seat in car
        public int getNumberSeat(string number)
        {
            int numberSeat = _context.Cars.Where(x => x.Number == number).Count();
            return numberSeat;
        }
        //get schdule 
        public int getSchedule(int carId)
        {
            int scheduleIds = _context.Schedules
                           .Where(s => s.Company.Cars.Any(car => car.CarId == carId))
                           .Select(s => s.ScheduleId).FirstOrDefault();
            return scheduleIds;


        }
        //create ticket
        public void createTicket(TicketDTO ticketDTO, int number, int carId)
        {
            for (int i = 1; i <= number; i++)
            {
                TicketDTO tickets = new TicketDTO();
                var ticket = _mapper.Map<Ticket>(tickets);
                ticket.CarId = carId;
                ticket.UserId = 1;
                ticket.DateFrom = ticketDTO.DateFrom;
                ticket.DateTo = ticketDTO.DateTo;
                ticket.TimeFrom = ticketDTO.TimeFrom;
                ticket.TimeTo = ticketDTO.TimeTo;
                ticket.Price = ticketDTO.Price;
                ticket.ScheduleId = getSchedule(carId);
                ticket.Status = "UnBook";
                ticket.SeatCode = "A" + i;
                _context.Add(ticket);
                _context.SaveChanges();

            }
        }
        //changeStatusTicket
        public void BookOrCancleTicket(int ticketID)
        {
            if (ticketID != null)
            {
                var getTicketById = _context.Tickets.FirstOrDefault(x => x.TicketId == ticketID);
                getTicketById.Status = getTicketById.Status == "UnBook" ? "Booked" : "UnBook";
                _context.SaveChanges();
            }
        }
        public List<Location> GetStartLocation(List<string> locations)
        {
            List<Location> result = new List<Location>();
            for (int i = 0; i < locations.Count; i++)
            {
                string startLocation = locations[i].Split(',')[0].Trim();
                result.Add(new Location { id = i + 1, value = startLocation });
            }
            return result;
        }


        public List<Location> GetEndLocation(List<string> locations)
        {
            List<Location> result = new List<Location>();
            for (int i = 0; i < locations.Count; i++)
            {
                string startLocation = locations[i].Split(',')[1].Trim();
                result.Add(new Location { id = i + 1, value = startLocation });
            }
            return result;
        }
        public string ConvertToJson(List<Location> locationItems)
        {
            var jsonResult = JsonConvert.SerializeObject(locationItems, Formatting.Indented);
            return jsonResult;
        }
        public List<string> GetLocation()
        {
            List<string> Locations = new List<string>();
            var schedules = _context.Schedules.ToList();
            foreach (var schedule in schedules)
            {
                string location = schedule.Location;
                Locations.Add(location);
            }
            return Locations;
        }
        public List<OutBookTicket> getTicketByRequire(string startLocation, string endLocation, DateTime dateFrom)
        {
            var tickets = _context.Tickets
                      .Include(x => x.Schedule).ThenInclude(c => c.Company).ThenInclude(x => x.Cars)
                      .Where(x => x.DateFrom.HasValue && x.DateFrom.Value.Date == dateFrom.Date
                          && x.Schedule.Location.Contains(startLocation)
                          && x.Schedule.Location.Contains(endLocation))
                      .ToList();

            var groupedTickets = tickets
                .GroupBy(t => new { CarId=  t.Car.CarId , Name =  t.Schedule.Company.Name})
                .Select(group => new OutBookTicket
                {
                    Price = group.First().Price,
                    TimeFrom = group.First().TimeFrom,
                    TimeTo = group.First().TimeTo,
                    SeatCode = group.Select(t => new Select { id = t.TicketId, value = t.SeatCode }).ToList(),
                    DateFrom = group.First().DateFrom,
                    DateTo = group.First().DateTo,
                    startLocation = startLocation,
                    endLocation = endLocation,
                    Name = group.Key.Name,
                    CarId = group.Key.CarId
                })
                .ToList();

            return groupedTickets;
        }




    }
}
