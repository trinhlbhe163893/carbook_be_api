﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using MyAPI.Dtos.CompaniesDTO;
using MyAPI.Models;

namespace MyAPI.DAO
{
    public class CompanyDAO
    {
        private readonly DB_CarBookingContext _context;
        private readonly IMapper _mapper;
        public CompanyDAO(DB_CarBookingContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public List<CompanyDTO> GetCompanies()
        {
            var companies = _context.Companies.Include(x => x.Manager).Select(
                c => new CompanyDTO
                {
                    Description = c.Description,
                    Name = c.Name,
                    Location = c.Location,
                    Phone = c.Phone,
                    ManagerID = c.Manager.UserId,
                    Status = c.Status,
                    UserName = c.Manager.UserName
                }).ToList();
            return companies;
        }

        public Company GetCompanyById(int id)
        {
            var company = _context.Companies.FirstOrDefault(x => x.CompanyId == id);
            return company;
        }
        public void updateCompanyById(int companyId, CompanyDTO companyDTO)
        {
            var company = GetCompanyById(companyId);
            if (company != null)
            {
                company.Description = companyDTO.Description;
                company.Location = companyDTO.Location;
                company.Name = companyDTO.Name;
                company.Phone = companyDTO.Phone;
                company.Status = companyDTO.Status;

                _context.Companies.Update(company);
                _context.SaveChanges();
            }
            else
            {
                throw new ArgumentException($"Company with ID {companyId} not found.");
            }
        }
        public bool checkNameCompanyExist(CompanyDTO companyDTO)
        {
            var company = _context.Companies.FirstOrDefault(x => x.Name == companyDTO.Name);
            if (company == null)
            {
                return true;
            }
            return false;
        }
        public void addCompany(CompanyDTO companyDTO)
        {
            var company = _mapper.Map<Company>(companyDTO);
          
            _context.Companies.Add(company);
            _context.SaveChanges();
        }
        public void ChangeStatusCompany(int companyId)
        {
            var company = GetCompanyById(companyId);
            if (company != null)
            {
                company.Status = company.Status == "Active" ? "Deactive" : "Active";
                _context.Companies.Update(company);
                _context.SaveChanges();
            }
            else
            {
                throw new ArgumentException($"Company with ID {companyId} not found.");
            }
        }
    }
}
