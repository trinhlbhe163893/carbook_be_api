﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using MyAPI.Models;
using MyAPI.Dtos.UserDTO.UserLogin;
using MyAPI.Dtos.UserDTO.UserRegister;
using System.IdentityModel.Tokens.Jwt;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using MyAPI.Dtos.UserDTO;

namespace MyAPI.DAO
{
    public class UserDAO
    {
        private readonly DB_CarBookingContext _context;
        private readonly IMapper _mapper;
        private readonly IConfiguration _configuration;
        public UserDAO(DB_CarBookingContext context, IMapper mapper, IConfiguration configuration)
        {
            _context = context;
            _mapper = mapper;
            _configuration = configuration;
        }
        public bool checkLogin(InputUserLoginDTO inUserDTO)
        {
            var checkAccount = _context.Users.FirstOrDefault(x => x.UserName == inUserDTO.username && x.Password == inUserDTO.password);
            if (checkAccount == null)
            {
                return false;
            }
            return true;
        }
        public class UserWithRolesDTO
        {
            //public string UserName { get; set; } = null!;
            public string? Email { get; set; }
            public List<int> RoleIds { get; set; } = new();
        }
        //public UserWithRolesDTO getUserByEmailOrName(InputUserLoginDTO inputUser)
        //{
        //    var userWithRoles = _context.Users
        // .Where(x => x.Email == inputUser.Email)
        // .Include(x => x.UserRoles)
        // .Select(x => new UserWithRolesDTO
        // {
        //     //UserName = x.UserName,
        //     Email = x.Email,
        //     RoleIds = x.UserRoles.Select(ur => ur.RoleId).ToList()
        // })
        // .FirstOrDefault();

        //    return userWithRoles;

        public OutUserLoginDTO getUserByEmailOrName(InputUserLoginDTO inputUser)
        {
            var user = _context.Users.Include(x => x.UserRoles).FirstOrDefault(x => x.Email == inputUser.username);
            var mappingUser = _mapper.Map<OutUserLoginDTO>(user);
            return mappingUser;
        }
        public bool checkEmailAndUserName(InUserRegister inputUserReg)
        {
            var user = _context.Users.FirstOrDefault(x => x.UserName == inputUserReg.UserName || x.Email == inputUserReg.Email);
            if (user == null) 
            {
                return true;
            }
            return false;
        }
        public void registerUser(InUserRegister inUserRegister)
        {
            var data = _mapper.Map<User>(inUserRegister);
            _context.Users.Add(data);
            _context.SaveChanges();
            var userRole = 1;
            UserRole ur = new UserRole
            {
                UserId = data.UserId,
                RoleId = userRole,
                Status = "active"
            };
            _context.UserRoles.Add(ur);
            _context.SaveChanges();
        }

        public async Task<OutUserLoginDTO> Login(string username, string password)
        {
            User? user = _context.Users
            .Include(x => x.UserRoles)
            .FirstOrDefault(x => (x.Email == username || x.UserName == username) && x.Password == password);
            var userDTO = _mapper.Map<OutUserLoginDTO>(user); 
            if (userDTO == null) {
                return null;
            }

            var roleIdQuery = from u in _context.Users
                              join ur in _context.UserRoles on u.UserId equals ur.UserId
                              where u.UserName == username
                              select ur.RoleId;
            // Lấy một danh sách các RoleId
            List<int> roleIdList = await roleIdQuery.ToListAsync();

            // Hoặc lấy một RoleId đầu tiên
            int? roleId = await roleIdQuery.FirstOrDefaultAsync();
            Console.WriteLine("role ID user : " + roleId);
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_configuration["JWT:SecretKey"]);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                      new Claim(ClaimTypes.Email, userDTO.Email),
                      new Claim(ClaimTypes.Role, roleId.ToString()),
                      new Claim("ID", user.UserId.ToString())
                }),
                IssuedAt = DateTime.UtcNow,
                Issuer = _configuration["JWT:Issuer"],
                Audience = _configuration["JWT:Audience"],
                Expires = DateTime.UtcNow.AddMinutes(30),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature),
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            userDTO.Token = tokenHandler.WriteToken(token);

            return userDTO;
        }
        public bool checkDuplicateEmailOrPhone(int id, string email, string phone)
        {
            var user = _context.Users.FirstOrDefault(x => (x.UserId != id) && (x.Email == email || x.Mobile == phone));
            return user != null;
        }
        public void editUser(int id, UserDTO user)
        {
            var userById = GetUserProfileById(id);
            userById.UserId = id;
            userById.Password = user.Password;
            userById.UserName = user.UserName;
            userById.Email = user.Email;
            userById.Mobile = user.Mobile;
            userById.Picture = user.Picture;
            userById.Address = user.Address;
            userById.City = user.City;
            userById.Dob = user.Dob;
            var userupdate = _mapper.Map<User>(userById);
            _context.Update(userupdate);
            _context.SaveChanges();
        }
        public UserDTO GetUserProfileById(int id)
        {
            var user = (from u in _context.Users
                        join ur in _context.UserRoles on u.UserId equals ur.UserId
                        where u.UserId == id
                        select new UserDTO
                        {
                            UserId = u.UserId,
                            UserName = u.UserName,
                            Email = u.Email,
                            Mobile = u.Mobile,
                            Picture = u.Picture,
                            Dob = u.Dob,
                            Address = u.Address,
                            City = u.City,
                            RoleId = ur.RoleId
                        }).FirstOrDefault();

            return user;
        }
        public List<UserCompanyDTO> GetUserRoleCompany()
        {
            var users = _context.Users
                .Include(u => u.UserRoles)
                    .ThenInclude(ur => ur.Role)
                .Where(u => u.UserRoles.Any(ur => ur.Role.RoleName.Equals("Company")))
                .Select(u => new UserCompanyDTO
                {
                    ID = u.UserId,
                    Value = u.UserName
                })
                .ToList();

            return users;
        }
    }
}
