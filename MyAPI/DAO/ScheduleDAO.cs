﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using MyAPI.Dtos.CompaniesDTO;
using MyAPI.Dtos.SchduleDTO;
using MyAPI.Dtos.ScheduleDTO;
using MyAPI.Models;

namespace MyAPI.DAO
{
    public class ScheduleDAO
    {

        private readonly DB_CarBookingContext _context;
        private readonly IMapper _mapper;
        public ScheduleDAO(DB_CarBookingContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public List<SchdeuleListDTO> GetSchedules()
        {
            var listSchedule = _context.Schedules.Include(s => s.Company).ToList();
            var listScheduleDTO = listSchedule.Select(s => new SchdeuleListDTO
            {
                CompanyId = s.CompanyId,
                CompanyName = s.Company.Name,
                Name = s.Name,
                Location = s.Location,
                Status = s.Status,  
            }).ToList();
            return listScheduleDTO;
        }
        public Schedule GetScheduleById(int id)
        {
            var schedule = _context.Schedules.FirstOrDefault(x => x.ScheduleId == id);
            return schedule;
        }
        public void updateScheduleById(int scheduleId, ScheduleDTO scheduleDTO)
        {
            var schedule = GetScheduleById(scheduleId);
            if (schedule != null)
            {
                schedule.Location = scheduleDTO.StartLocation + "-" + scheduleDTO.EndLocation;
                schedule.Status = scheduleDTO.Status;
                schedule.Name = scheduleDTO.Name;
                schedule.CompanyId = scheduleDTO.CompanyId;

                _context.Schedules.Update(schedule);
                _context.SaveChanges();
            }
            else
            {
                throw new ArgumentException($"Schedule with ID {scheduleId} not found.");
            }
        }
        public bool checkNameScheduleExist(ScheduleDTO scheduleDTO)
        {
            var schedule = _context.Schedules.FirstOrDefault(x => x.Name == scheduleDTO.Name);
            if (schedule == null)
            {
                return true;
            }
            return false;
        }
        public void addSchedule(ScheduleDTO scheduleDTO)
        {
            var schedule = new Schedule
            {
                CompanyId = scheduleDTO.CompanyId,
                Location = scheduleDTO.StartLocation + "-"+ scheduleDTO.EndLocation,
                Status = scheduleDTO.Status,
                Name = scheduleDTO.Name,
            };

            _context.Schedules.Add(schedule);
            _context.SaveChanges();
        }
        public void ChangeStatusSchedule(int scheduleId)
        {
            var schedule = GetScheduleById(scheduleId);
            if (schedule != null)
            {
                schedule.Status = schedule.Status == "Active" ? "Deactive" : "Active";
                _context.Schedules.Update(schedule);
                _context.SaveChanges();
            }
            else
            {
                throw new ArgumentException($"Company with ID {scheduleId} not found.");
            }
        }
    }
}
