﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using MyAPI.Dtos.CarDTO;
using MyAPI.Dtos.SchduleDTO;
using MyAPI.Models;

namespace MyAPI.DAO
{
    public class CarDAO
    {

        private readonly DB_CarBookingContext _context;
        private readonly IMapper _mapper;
        public CarDAO(DB_CarBookingContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public List<CompanyCarDTO> getCompany()
        {
            var listCompany = _context.Companies.ToList();
            var listCompanyDTO = listCompany.Select(s => new CompanyCarDTO
            {
                id = s.CompanyId,
                value = s.Name
            }).ToList();
            return listCompanyDTO;
        }
        public List<CarListDTO> GetListCar()
        {
            var listCar = _context.Cars.Include(s => s.Company).ToList();
            var listCarDTO = listCar.Select(s => new CarListDTO
            {
                CompanyId = s.CompanyId,
                CompanyName = s.Company.Name,
                Number = s.Number,
                SeatCode = s.SeatCode,
                Phone = s.Phone,
                Status = s.Status,
                TypeId = s.TypeId,

            }).ToList();
            return listCarDTO;
        }
        public Car GetCarById(int id)
        {
            var car = _context.Cars.FirstOrDefault(x => x.CarId == id);
            return car;
        }
        public void updateCarById(int carId, CarDTO carDTO)
        {
            var car = GetCarById(carId);
            if (car != null)
            {
                int seat_Code = Int32.Parse(carDTO.SeatCode);
                List<string> seat_CodeList = new List<string>();

                for (int i = 1; i <= seat_Code; i++)
                {
                    seat_CodeList.Add(i.ToString("D2"));
                }

                string seatCodeAfterParse = string.Join(",", seat_CodeList);

                car.CompanyId = carDTO.CompanyId;
                car.Number = carDTO.Number;
                car.SeatCode = seatCodeAfterParse;
                car.Phone = carDTO.Phone;
                car.TypeId = carDTO.TypeId;
                car.Status = carDTO.Status;

                _context.Cars.Update(car);
                _context.SaveChanges();
            }
            else
            {
                throw new ArgumentException($"Car with ID {carId} not found.");
            }
        }

        public bool checkCarExistByNumber(string Number)
        {
            var car = _context.Cars.FirstOrDefault(x => x.Number == Number);
            if (car == null)
            {
                return true;
            }
            return false;
        }
        public void addCar(CarDTO carDTO)
        {
            int seat_Code = Int32.Parse(carDTO.SeatCode);
            List<String> seat_CodeList = new List<String>();    

            for (int i = 1; i <= seat_Code; i++)
            {
                seat_CodeList.Add(i.ToString("D2"));
            }
            string seatCodeAfterParse = string.Join(",", seat_CodeList);

            var car = new Car
            {
                CompanyId = carDTO.CompanyId,
                Number = carDTO.Number,
                SeatCode = seatCodeAfterParse,
                Phone = carDTO.Phone,
                TypeId = carDTO.TypeId,
                Status = carDTO.Status,
            };

            _context.Cars.Add(car);
            _context.SaveChanges();
        }
        public void ChangeStatusCar(int carID)
        {
            var car = GetCarById(carID);
            if (car != null)
            {
                car.Status = car.Status == "Active" ? "Deactive" : "Active";
                _context.Cars.Update(car);
                _context.SaveChanges();
            }
            else
            {
                throw new ArgumentException($"Car with ID {carID} not found.");
            }
        }
    }
}
