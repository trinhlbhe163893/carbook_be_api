﻿using AutoMapper;
using MyAPI.Dtos.CarDTO;
using MyAPI.Dtos.CompaniesDTO;
using MyAPI.Dtos.RoleDTO;
using MyAPI.Dtos.SchduleDTO;
using MyAPI.Dtos.TicketDTO;
using MyAPI.Dtos.UserDTO;
using MyAPI.Dtos.UserDTO.UserLogin;
using MyAPI.Dtos.UserDTO.UserRegister;
using MyAPI.Models;

namespace MyAPI.MappingProfiles
{
    public class AutoMappings : Profile
    {
        public AutoMappings()
        {
            CreateMap<User, UserDTO>().ReverseMap();
            CreateMap<UserDTO, User>().ReverseMap();
            CreateMap<AddUserDTO, User>().ReverseMap();
            CreateMap<User, InputUserLoginDTO>().ReverseMap();
            CreateMap<User, OutUserLoginDTO>().ReverseMap();
            //CreateMap<UserRole,UserRoleDTO>().ReverseMap();
            CreateMap<UserRole, UserRoleDTO>()
           .ForMember(dest => dest.RoleId, opt => opt.MapFrom(src => src.RoleId));
            CreateMap<User, InUserRegister>().ReverseMap();
            CreateMap<Company,CompanyDTO>().ReverseMap();
            CreateMap<TicketDTO, Ticket>().ReverseMap();
            CreateMap<Ticket, OutBookTicket>().ReverseMap();
            CreateMap<Schedule, ScheduleDTO>().ReverseMap();
            CreateMap<Car, CarDTO>().ReverseMap();
        }
    }
}
